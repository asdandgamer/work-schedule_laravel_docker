**STABLE SETTINGS OF**
- Docker-composer
- NGINX
- PHP 7.4 FPM
- Mysql 5.7

__Installation__

Put **composer.phar** to ./docker_config/php/

Run command:
```
docker-compose up -d --build
```
connect to php in terminal via command:
```
docker-compose exec php sh
```
increase composer timeout
```
export COMPOSER_PROCESS_TIMEOUT=900
```
create laravel project:
```
composer create-project --prefer-dist laravel/laravel work_schedule "5.6.*"
```
if there would be an error of "installable set of packages" go to folder and install with ignoration:
```
composer install --ignore-platform-reqs
```
generate key:
```
php artisan key:generate
```
set the database connection in laravel .env file:
```
DB_HOST=db
DB_PORT=3306
DB_DATABASE=sitedb
DB_USERNAME=root
DB_PASSWORD=123456
```
 migrate database:
```
php artisan migrate
```

Based on 
https://youtu.be/tXg24aFcp-0
YT CHANNEL Lessons DKA-DEVELOP(Laravel + Docker)
https://youtu.be/TumfGqUf39U
Backend TV https://youtu.be/2wGEirrpWsU